<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $to = "profmenuis@gmail.com"; // Remplacez par votre adresse email
    $subject = "Auto-évaluation PFMP";
    $competences = $_POST['competences'];
    $satisfaction = $_POST['satisfaction'];
    $ameliorations = $_POST['ameliorations'];

    $message = "Compétences acquises :\n$competences\n\n";
    $message .= "Satisfaction personnelle : $satisfaction/10\n\n";
    $message .= "Suggestions d'amélioration :\n$ameliorations";

    $headers = "From: formulaire@votre-domaine.com"; // Remplacez par votre domaine

    if (mail($to, $subject, $message, $headers)) {
        echo "Votre auto-évaluation a été envoyée avec succès.";
    } else {
        echo "Erreur lors de l'envoi de l'auto-évaluation. Veuillez réessayer.";
    }
}
?>

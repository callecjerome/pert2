let taskCounter = 0;  // Compteur pour les lettres (A, B, C, etc.)

// Fonction pour ajouter une tâche avec une étiquette
function addTask() {
    // Récupérer la liste des tâches
    const taskList = document.getElementById('taskList');
    
    // Créer un nouvel élément de tâche
    const taskItem = document.createElement('li');
    
    // Générer une lettre basée sur le compteur
    const letter = String.fromCharCode(65 + taskCounter);  // 65 est le code ASCII pour 'A'
    
    // Ajouter l'étiquette et la tâche
    taskItem.innerHTML = `<span class="task-label">${letter}</span> Tâche ${taskCounter + 1}`;
    
    // Ajouter l'élément à la liste
    taskList.appendChild(taskItem);
    
    // Incrémenter le compteur pour la prochaine tâche
    taskCounter++;
}

// Ajouter un écouteur d'événement pour le bouton "Ajouter une tâche"
document.getElementById('addTaskBtn').addEventListener('click', addTask);
